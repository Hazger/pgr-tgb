#ifndef CAMERA_H
#define CAMERA_H

#include <math.h>
#define PI 3.14159265


class Camera{
private:
	double angle;
	
	double dx;
	double dy;
	double dz;

	float fx;
	float fy;
	float fz;
public:
	Camera(){
		this->angle = 0;
                
		this->dx = 1;
		this->dy = 0;
		this->dz = 0;
	
		this->fx = 0;
		this->fy = 0;
		this->fz = 0;
	}
	void checkPosition(){
		// Verifica se a posição do dx nao está fora dos limites
		if (this->fx > 99){
			this->fx = 99;
		}else if(this->fx < -99){
			this->fx = -99;
		}
		if (this->fz > 99){
			this->fz = 99;
		}else if (this->fz < -99){
			this->fz = -99;
		}
	}
	void turnAround(){
		this->angle += 180;
		if(angle > 360){
			angle -= 360;
		}
	}
	void turnLeft(){
		if (this->angle == 0){
			this->angle = 360;
		}
		this->angle--;
	}
	void turnRight(){
		if (this->angle == 360){
			this->angle = 0;
		}
		this->angle++;
	}
	void goForward(){
		this->fx += 0.1 * this->dx;
		this->fz += 0.1 * this->dz;
		checkPosition();
	}
	void goBack(){
		this->fx -= 0.1 * this->dx;
		this->fz -=  0.1 * this->dz;
		checkPosition();
    }
    void goRight(){           
		this->fx -= 0.1*this->dz;
		this->fz += 0.1*this->dx;
		checkPosition();
    }
    void goLeft(){
		this->fx += 0.1*this->dz;
		this->fz -= 0.1*this->dx;
		checkPosition();
    }
    void recalcSenCos(){
		this->dx = cos(this->angle * PI / 180);
		this->dz = sin(this->angle * PI / 180);
    }

    double getAngle(){ return this->angle; }
    double getDx(){ return this->dx; }
	double getDy() { return this->dy; }
    double getDz() { return this->dz; }
    float getFx() { return this->fx; }
    float getFy() { return this->fy; }
    float getFz(){ return this->fz; }

	void reset() {
		this->angle = 0;
                
		this->dx = 1;
		this->dy = 0;
		this->dz = 0;
	
		this->fx = 0;
		this->fy = 0;
		this->fz = 0;
	}
};

#endif