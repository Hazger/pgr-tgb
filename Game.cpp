#define _CRT_SECURE_NO_DEPRECATE
#include <windows.h>
#include "Game.h"

// Inicia uma rodada do jogo
void Game::init() {
	// Inicia a contagem do tempo do jogo
	this->timer->start();
	// Jogo em execu��o
	this->running = true;
	// Mapa do jogo ativo
	this->mapActive = true;
}

// Reseta uma rodada do jogo (volta para o in�cio do jogo com o mesmo cen�rio)
// Utilizado quando o jogador ainda tem vidas
void Game::reset() {

	// Volta a c�mera para o in�cio do jogo
	this->camera->reset();
	
	// Volta o cen�rio (apenas os tiros, neste caso)
	this->scenario->reset();

	// Volta jogador para o in�cio
	glLoadIdentity();
		gluLookAt(
				this->getCamera()->getFx(),
				this->getCamera()->getFy(),
				this->getCamera()->getFz(), 
				this->getCamera()->getFx()+this->getCamera()->getDx(),
				this->getCamera()->getFy()+this->getCamera()->getDy(), 
				this->getCamera()->getFz()+this->getCamera()->getDz(), 
				0,1,0
		);

	// Para o jogo
	this->running = false;
	// Esconde mapa do jogo
	this->mapActive = false;
}

// Opera��es executadas a cada loop da glut
void Game::tick(){
	// Verifica se o tempo acabou
	if (this->timer->getRemainingTime(GAME_TIME) <= 0 && this->lives != 0) {
		// Diminui uma vida
		this->lives--;
		// Volta para o in�cio do jogo
		reset();
	}

	// Verifica colis�es
	int collision = checkCollision(this->getCamera()->getFx(), this->getCamera()->getFz(), 0.2, 0.2, true);
	if (collision == 2 || collision == 3) {

		// Mostra a mensagem de colis�o
		MessageUtil::showCollision();

		// Diminui uma vida
		this->lives--;

		// Volta para o in�cio do jogo
		reset();

	} else if (collision == 1) {
		// Colis�o com um sobrevivente
		if (this->getScenario()->getNumberOfActiveSurvivors() == 0) {
			// Se o jogador resgatou o �ltimo sobrevivente, reinicia o jogo
			reset();
		}

	}

	// Verifica colis�es dos tiros
	for (int i = 0; i < this->scenario->getNextShotToLoad(); i++) {
		if (this->scenario->getShots()[i]->isActive()) {
			int shotCollision = checkCollision(this->scenario->getShots()[i]->getX(), this->scenario->getShots()[i]->getZ(), this->scenario->getShots()[i]->getWidth(), this->scenario->getShots()[i]->getDepth(), true);
			if (shotCollision == 1) { // Colis�o com um sobrevivente
				// Diminui uma vida
				this->lives--;
				// Volta para o in�cio do jogo
				reset();
				MessageUtil::showSurvivorShot();
			} else if (shotCollision != 0) { // Outras colis�es
				// Desativa o tiro
				this->scenario->getShots()[i]->setActive(false);
			}
		}
	}

	// Move os aster�ides pelo cen�rio
	moveAsteroids();
	
	// Desenha o cen�rio
	this->scenario->draw();

	// Move os tiros pelo cen�rio
	this->scenario->moveShots();

	// Desenha o mapa
	if (this->mapActive) {
		this->drawMap();
	}

	// Mostra informa��es do jogo
	if (this->isRunning()) {
		MessageUtil::showGameInfo(this->getLives(), this->getScenario()->getNumberOfActiveSurvivors(), this->getTimer()->getRemainingTime(GAME_TIME), this->isRunning());
	} else {
		MessageUtil::showGameInfo(this->getLives(), this->getScenario()->getNumberOfActiveSurvivors(), GAME_TIME, this->isRunning());
	}
}

// Move os aster�ides pelo cen�rio
void Game::moveAsteroids(){
	
	float asteroidMov = 0.001;
	for (int i = 0; i < this->getScenario()->getNextAsteroidToLoad(); i++) {
		if (this->getScenario()->getAsteroids()[i]->isActive()) {			
			float astX = this->getScenario()->getAsteroids()[i]->getX();
			float astZ = this->getScenario()->getAsteroids()[i]->getZ();
			if (astX < -10 || astZ < -10 || astX > 10 || astZ > 10){
				this->getScenario()->getAsteroids()[i]->reverseMovement();
			}
			for (int ii = 0; ii < this->getScenario()->getNextAsteroidToLoad(); ii++){
				if(astX != this->getScenario()->getAsteroids()[ii]->getX() &&  astZ != this->getScenario()->getAsteroids()[ii]->getZ() 
									&& this->getScenario()->getAsteroids()[ii]->isActive()){ //se o x e Z forem diferentes e estiver ativo
					if (collision (astX, astZ, ENEMY_WIDTH, ENEMY_DEPTH, this->getScenario()->getAsteroids()[ii]->getX(), this->getScenario()->getAsteroids()[ii]->getZ(), ENEMY_WIDTH, ENEMY_DEPTH ) ){
						// Eles colidiram
						this->getScenario()->getAsteroids()[i]->reverseMovement();
					}
				}
			}			
			for (int ii = 0; ii < this->getScenario()->getNextEnemyToLoad(); ii++){
				if(this->getScenario()->getEnemies()[ii]->isActive()){ //se o x e Z forem diferentes e estiver ativo
					if (collision (astX, astZ, ENEMY_WIDTH, ENEMY_DEPTH, this->getScenario()->getEnemies()[ii]->getX(), this->getScenario()->getEnemies()[ii]->getZ(), ENEMY_WIDTH, ENEMY_DEPTH ) ){
						// Eles colidiram
						this->getScenario()->getAsteroids()[i]->reverseMovement();
					}
				}
			}			
			for (int ii = 0; ii < this->getScenario()->getNextSurvivorToLoad(); ii++){
				if(this->getScenario()->getSurvivors()[ii]->isActive()){ //se o x e Z forem diferentes e estiver ativo
					if (collision (astX, astZ, ENEMY_WIDTH, ENEMY_DEPTH, this->getScenario()->getSurvivors()[ii]->getX(), this->getScenario()->getSurvivors()[ii]->getZ(), ENEMY_WIDTH, ENEMY_DEPTH ) ){
						// Eles colidiram
						this->getScenario()->getAsteroids()[i]->reverseMovement();
					}
				}
			}
			
			// Agora que j� viu, se bate e se move
			switch (this->getScenario()->getAsteroids()[i]->getMovDirection()){
			case 1:
				this->getScenario()->getAsteroids()[i]->setX(astX + asteroidMov);
				break;
			case 2:
				this->getScenario()->getAsteroids()[i]->setX(astX - asteroidMov);
				break;
			case 3:
				this->getScenario()->getAsteroids()[i]->setZ(astZ + asteroidMov);
				break;
			case 4:
				this->getScenario()->getAsteroids()[i]->setZ(astZ - asteroidMov);
				break;
			default:
				break;
			}
		}
	}
}

// Carrega o cen�rio: escolhe a quantidade e localiza��o das naves inimigas,
// sobreviventes e aster�ides
void Game::loadScenario() {

	srand (time(0));

	// Carrega os sobreviventes
	int numberOfSurvivors = rand() % 21;
	while (numberOfSurvivors == 0) {
		numberOfSurvivors = rand() % 21;
	}
	this->scenario->setNumberOfSurvivors(numberOfSurvivors);
	for (int i = 0; i < numberOfSurvivors; i++) {
		Character* survivor = new Character(SURVIVOR);
		survivor->setHeight(SURVIVOR_HEIGHT);
		survivor->setWidth(SURVIVOR_WIDTH);
		survivor->setDepth(SURVIVOR_DEPTH);
		survivor->setX(((float)rand()/(float)(RAND_MAX)) * 20 - 10); // Sorteia um n�mero de -10 a 10
		survivor->setY(GAME_Y);
		survivor->setZ(((float)rand()/(float)(RAND_MAX)) * 20 - 10);
		if (checkCollision(survivor->getX(), survivor->getZ(), survivor->getWidth(), survivor->getDepth(), false) == 4) {
			survivor->setActive(false);
			this->scenario->decNumberOfActiveSurvivors();
		}
		this->scenario->addSurvivor(survivor);
	}

	// Carrega os inimigos
	int numberOfEnemies = rand() % 21;
	this->scenario->setNumberOfEnemies(numberOfEnemies);
	for (int i = 0; i < numberOfEnemies; i++) {
		Character* enemy = new Character(ENEMY);
		enemy->setHeight(ENEMY_HEIGHT);
		enemy->setWidth(ENEMY_WIDTH);
		enemy->setDepth(ENEMY_DEPTH);
		enemy->setX(((float)rand()/(float)(RAND_MAX)) * 20 - 10); // Sorteia um n�mero de -10 a 10
		enemy->setY(GAME_Y);
		enemy->setZ(((float)rand()/(float)(RAND_MAX)) * 20 - 10);
		if (checkCollision(enemy->getX(), enemy->getZ(), enemy->getWidth(), enemy->getDepth(), false) == 4) {
			enemy->setActive(false);
			this->scenario->decNumberOfActiveEnemies();
		}
		this->scenario->addEnemy(enemy);
	}

	// Carrega os aster�ides
	int numberOfAsteriods = rand() % 21;
	this->scenario->setNumberOfAsteroids(numberOfAsteriods);
	for (int i = 0; i < numberOfAsteriods; i++) {
		float size = ((0.8-0.4)*((float)rand()/RAND_MAX))+0.4;
		Character* asteroid = new Character(ASTEROID);
		asteroid->setHeight(size);
		asteroid->setWidth(size);
		asteroid->setDepth(size);
		asteroid->setX(((float)rand()/(float)(RAND_MAX)) * 20 - 10); // Sorteia um n�mero de -10 a 10
		asteroid->setY(GAME_Y);
		asteroid->setZ(((float)rand()/(float)(RAND_MAX)) * 20 - 10);
		asteroid->setMovDirection(rand() % 4 + 1);
		if (checkCollision(asteroid->getX(), asteroid->getZ(), asteroid->getWidth(), asteroid->getDepth(), false) == 4) {
			asteroid->setActive(false);
			this->scenario->decNumberOfActiveAsteroids();
		}
		this->scenario->addAsteroid(asteroid);
	}
}

// Verifica a colis�o entre dois objetos
bool Game::collision(float x1, float z1, float width1, float depth1, float x2, float z2, float width2, float depth2){
	//std::cout << "x1:" << x1 << " z1:" << z1 << std::endl;
	if (x1+width1 >= x2 && x1-width1 <= x2+width2){ // Colidiu em x
		if(z1+depth1 >= z2 && z1+depth1 <= z2+depth2+0.2){ // Colidiu em z
			//std::cout << "x2:" << x2 << " z2:" << z2 << std::endl;
			//std::cout << "x1:" << x1 << " z1:" << z1 << std::endl;
			return true;
		}		
	}
	return false;
}

// Verifica a colis�o entre um objeto e todo o cen�rio
int Game::checkCollision(float x1, float z1, float width, float depth, bool isCamera){
	
	for (int i = 0; i < this->getScenario()->getNextSurvivorToLoad(); i++) {
		if (this->getScenario()->getSurvivors()[i]->isActive()) {			
			if (collision(x1, z1, width, depth, this->getScenario()->getSurvivors()[i]->getX(), this->getScenario()->getSurvivors()[i]->getZ(), this->getScenario()->getSurvivors()[i]->getWidth(), this->getScenario()->getSurvivors()[i]->getDepth())){
				this->getScenario()->getSurvivors()[i]->setActive(false);
				this->getScenario()->decNumberOfActiveSurvivors();
				return 1;
			}
		}
	}

	for (int i = 0; i < this->getScenario()->getNextEnemyToLoad(); i++) {
		if (this->getScenario()->getEnemies()[i]->isActive()) {			
			if (collision(x1, z1, width, depth, this->getScenario()->getEnemies()[i]->getX(), this->getScenario()->getEnemies()[i]->getZ(), this->getScenario()->getEnemies()[i]->getWidth(), this->getScenario()->getEnemies()[i]->getDepth())){
				this->getScenario()->getEnemies()[i]->setActive(false);
				this->getScenario()->decNumberOfActiveEnemies();
				return 2;
			}
		}
	}

	for (int i = 0; i < this->getScenario()->getNextAsteroidToLoad(); i++) {
		if (this->getScenario()->getAsteroids()[i]->isActive()) {			
			if (collision(x1, z1, width, depth, this->getScenario()->getAsteroids()[i]->getX(), this->getScenario()->getAsteroids()[i]->getZ(), this->getScenario()->getAsteroids()[i]->getWidth(), this->getScenario()->getAsteroids()[i]->getDepth())){
				this->getScenario()->getAsteroids()[i]->setActive(false);
				this->getScenario()->decNumberOfActiveAsteroids();
				return 3;
			}
		}
	}

	// Verifica tamb�m a colis�o com a c�mera
	if (!isCamera) {
		if (collision(x1, z1, width, depth, this->getCamera()->getFx(), this->getCamera()->getFz(), 0.2, 0.2)) {
			return 4;
		}
	}

	return 0;
}

// Adiciona um tiro no cen�rio
void Game::shoot(){
	//std::cout << "O game atirou" << std::endl;
	
	Character* shot = new Character(SHOT);
	shot->setX(this->getCamera()->getFx());
	shot->setY(SHOT_Y);
	shot->setZ(this->getCamera()->getFz());
	shot->setDx(this->getCamera()->getDx());
	shot->setDz(this->getCamera()->getDz());
	shot->setWidth(SHOT_WIDTH);
	shot->setHeight(SHOT_HEIGHT);
	shot->setDepth(SHOT_DEPTH);

	this->scenario->addShot(shot);
}

// Desenha o mapa do jogo
void Game::drawMap() {
	// Salva as matrizes
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
    glLoadIdentity();
	
	// Altera para projec�o ortogr�fica
	gluOrtho2D(0, 10.0, 0, 10.0);
	
	glPointSize(4);
	glBegin(GL_POINTS);

	// Desenha os aster�ides
	glColor3f(0.5, 0.5, 0.5);
	for (int i = 0; i < this->getScenario()->getNextAsteroidToLoad(); i++) {
		if (this->getScenario()->getAsteroids()[i]->isActive()) {
			float x = ((this->getScenario()->getAsteroids()[i]->getX() + 10.0)/10.0) + 8.0;
			float y = ((this->getScenario()->getAsteroids()[i]->getZ() + 10.0)/10.0);
			if (x <= 10 && x >= 8 && y <= 2 && y >= 0) { // Verifica limites da godview
				glVertex3f(x, y, 1);
			}
		}
	}

	// Desenha os inimigos
	glColor3f(0.5, 0.0, 0.0);
	for (int i = 0; i < this->getScenario()->getNextEnemyToLoad(); i++) {
		if (this->getScenario()->getEnemies()[i]->isActive()) {
			float x = ((this->getScenario()->getEnemies()[i]->getX() + 10.0)/10.0) + 8.0;
			float y = ((this->getScenario()->getEnemies()[i]->getZ() + 10.0)/10.0);
			if (x <= 10 && x >= 8 && y <= 2 && y >= 0) { // Verifica limites da godview
				glVertex3f(x, y, 1);
			}
		}
	}

	// Desenha os sobreviventes
	glColor3f(0.0, 0.7, 0.0);
	for (int i = 0; i < this->getScenario()->getNextSurvivorToLoad(); i++) {
		if (this->getScenario()->getSurvivors()[i]->isActive()) {
			float x = ((this->getScenario()->getSurvivors()[i]->getX() + 10.0)/10.0) + 8.0;
			float y = ((this->getScenario()->getSurvivors()[i]->getZ() + 10.0)/10.0);
			if (x <= 10 && x >= 8 && y <= 2 && y >= 0) { // Verifica limites da godview
				glVertex3f(x, y, 1);
			}
		}
	}

	// Desenha a c�mera
	glColor3f(0.9, 0.9, 0.1);
	float x = ((this->getCamera()->getFx() + 10.0)/10.0) + 8.0;
	float y = ((this->getCamera()->getFz() + 10.0)/10.0);
	if (x <= 10 && x >= 8 && y <= 2 && y >= 0) { // Verifica limites da godview
		glVertex3f(x, y, 1);
	}

	glEnd();

	// Desenha o mapa
	glColor3f(0.1, 0.3, 0.5);
	glBegin(GL_POLYGON);
		glVertex3f(7.95, 2.05, 1);
		glVertex3f(10, 2.05, 1);
		glVertex3f(10, 0, 1);
		glVertex3f(7.95, 0, 1);
	glEnd();

	// Volta as matrizes
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

// Alterna a visualiza��o do mapa
void Game::toggleMapActive() {
	if (this->mapActive) {
		this->mapActive = false;
	} else {
		this->mapActive = true;
	}
}