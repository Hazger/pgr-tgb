#include <time.h>

#ifndef TIMER_H
#define TIMER_H

class Timer {
	private:
		unsigned long initTime;
	public:
		// Inicia o timer
		void start() {
			initTime = clock();
		}

		// Retorna o tempo em milisegundos
		unsigned long getElapsedTime() {
			return ((unsigned long) clock() - initTime) / (CLOCKS_PER_SEC/1000);
		}

		// Retorna o tempo restante em segundos de acordo com o tempo passado por par�metro
		unsigned long getRemainingTime(unsigned long seconds) {
			return seconds - (getElapsedTime()/1000);
		}

		// Verifica se o tempo em segundos recebido por par�metro j� se esgotou
		bool isTimeout(unsigned long seconds) {
			return seconds >= (getElapsedTime()/1000);
		}
};

#endif