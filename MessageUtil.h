#include "StringUtil.h"

#ifndef MESSAGEUTIL_H
#define MESSAGEUTIL_H

class MessageUtil {
	private:
	static void beforeShowMessage();
	static void afterShowMessage();
	static void showGameStart();
	static void showGameOver();
	static void showCongrats();
	static void showShortTime();

	public:
	static void showGameInfo(int lives, int survivors, int remainingTime, bool isGameRunning);
	static void showSurvivorShot();
	static void showCollision();

};

#endif