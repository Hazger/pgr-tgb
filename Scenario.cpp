#include "Scenario.h"
#include <iostream>

// Desenha o cen�rio completo
void Scenario::draw(){
	drawFloor();
	drawEnemies();
	drawSurvivors();
	drawAsteroids();
	drawShots();
}

// Reseta o cen�rio
void Scenario::reset() {
	// Reseta os tiros
	if (this->shots != NULL) { delete this->shots; }
	this->shots = new Character*[numberOfShots];
	this->nextShotToLoad = 0;
}

// Desenha os inimigos
void Scenario::drawEnemies() {
	for (int i = 0; i < nextEnemyToLoad; i++) {
		if (enemies[i]->isActive()) {
			enemies[i]->draw();
		}
	}
}

// Desenha os sobreviventes
void Scenario::drawSurvivors() {
	for (int i = 0; i < nextSurvivorToLoad; i++) {
		if (survivors[i]->isActive()) {
			survivors[i]->draw();
		}
	}
}

// Desenha os aster�ides
void Scenario::drawAsteroids() {
	for (int i = 0; i < nextAsteroidToLoad; i++) {
		if (asteroids[i]->isActive()) {
			asteroids[i]->draw();
		}
	}
}

// Desenha os tiros
void Scenario::drawShots() {
	for (int i = 0; i < nextShotToLoad; i++) {
		if (shots[i]->isActive()) {
			shots[i]->draw();
		}
	}
}

// Movimenta os tiros pelo cen�rio
void Scenario::moveShots() {
	for (int i = 0; i < nextShotToLoad; i++) {
		if (shots[i]->isActive()) {
			shots[i]->setX(shots[i]->getX() + (SHOT_SPEED * shots[i]->getDx()));
			shots[i]->setZ(shots[i]->getZ() + (SHOT_SPEED * shots[i]->getDz()));

			if (shots[i]->getX() > 10 || shots[i]->getX() < -10){ // Trata limite do cen�rio
				shots[i]->setActive(false);

			} else if (shots[i]->getZ() > 10 || shots[i]->getZ() < -10){ // Trata limite do cen�rio
				shots[i]->setActive(false);
			}
		}
	}
}

// Adiciona um inimigo no cen�rio
void Scenario::addEnemy(Character* newEnemy) {
	if (numberOfEnemies > nextEnemyToLoad) {
		enemies[nextEnemyToLoad] = newEnemy;
		nextEnemyToLoad++;
	}
}

// Adiciona um sobrevivente no cen�rio
void Scenario::addSurvivor(Character* newSurvivor) {
	if (numberOfSurvivors > nextSurvivorToLoad) {
		survivors[nextSurvivorToLoad] = newSurvivor;
		nextSurvivorToLoad++;
	}
}

// Adiciona um aster�ide no cen�rio
void Scenario::addAsteroid(Character* newAsteroid) {
	if (numberOfAsteroids > nextAsteroidToLoad) {
		asteroids[nextAsteroidToLoad] = newAsteroid;
		nextAsteroidToLoad++;
	}
}

// Adiciona um tiro no cen�rio
void Scenario::addShot(Character* newShot) {
	if (nextShotToLoad == numberOfShots) {
		nextShotToLoad = 0;
	}
	shots[nextShotToLoad] = newShot;
	nextShotToLoad++;
}

// Adiciona o ch�o no cen�rio
void Scenario::drawFloor(){
	glColor3f(1.0, 0.0, 0.0);//vermelho	
	glBegin(GL_POLYGON);
		glVertex3f(0, -0.2, 0);
		glVertex3f(0, -0.2, 10);
		glVertex3f(10, -0.2, 10);
		glVertex3f(10, -0.2, 0);
	glEnd();

	glColor3f(0.0, 1.0, 0.0);//verde
	glBegin(GL_POLYGON);
		glVertex3f(0, -0.2, 0);
		glVertex3f(0, -0.2, -10);
		glVertex3f(10, -0.2, -10);
		glVertex3f(10, -0.2, 0);
	glEnd();

	glColor3f(0.0, 0.0, 1.0);//azul
	glBegin(GL_POLYGON);
		glVertex3f(0, -0.2, 0);
		glVertex3f(0, -0.2, -10);
		glVertex3f(-10, -0.2, -10);
		glVertex3f(-10, -0.2, 0);
	glEnd();

	glColor3f(1.0, 1.0, 1.0);//branco
	glBegin(GL_POLYGON);
		glVertex3f(0, -0.2, 0);
		glVertex3f(0, -0.2, 10);
		glVertex3f(-10, -0.2, 10);
		glVertex3f(-10, -0.2, 0);
	glEnd();
}