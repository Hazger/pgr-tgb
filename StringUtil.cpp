#include <windows.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include "StringUtil.h"

// Escreve uma mensagem na tela.
// font: fonte com a qual a mensagem ser� escrita
// *str: mensagem
// strLength: tamanho da mensagem
// x, y: coordenadas onde o texto come�ar� a ser escrito
void StringUtil::writeMessage(void *font, char *str, int strLength, float x, float y){
	glRasterPos2d(x, y);
	for (int i = 0; i < strLength; i++) {
		glutBitmapCharacter(font, *(str++));
	}
}