#include <Windows.h>
#include <GL/gl.h>
#include "Character.h"

#define ENEMY_HEIGHT 0.5
#define ENEMY_DEPTH 0.5
#define ENEMY_WIDTH 0.5
#define SURVIVOR_HEIGHT 0.3
#define SURVIVOR_DEPTH 0.15
#define SURVIVOR_WIDTH 0.15
#define SHOT_HEIGHT 0.03
#define SHOT_DEPTH 0.03
#define SHOT_WIDTH 0.03
#define GAME_Y -0.2
#define SHOT_Y -0.1
#define SHOT_SPEED 0.2

#ifndef SCENARIO_H
#define SCENARIO_H

class Scenario{
private:
	Character **enemies;
	Character **survivors;
	Character **asteroids;
	Character **shots;
	int nextEnemyToLoad;
	int nextSurvivorToLoad;
	int nextAsteroidToLoad;
	int nextShotToLoad;

	// N�mero total de personagens
	int numberOfEnemies;
	int numberOfSurvivors;
	int numberOfAsteroids;
	int numberOfShots;

	// N�mero de personagens ativos
	int numberOfActiveEnemies;
	int numberOfActiveSurvivors;
	int numberOfActiveAsteroids;

	void drawFloor();
	void drawEnemies();
	void drawSurvivors();
	void drawAsteroids();
	void drawShots();

public:
	Scenario(){
		this->enemies = NULL;
		this->survivors = NULL;
		this->asteroids = NULL;

		this->numberOfEnemies = 0;
		this->numberOfSurvivors = 0;
		this->numberOfAsteroids = 0;
		this->numberOfShots = 20;

		this->nextEnemyToLoad = 0;
		this->nextSurvivorToLoad = 0;
		this->nextAsteroidToLoad = 0;
		this->nextShotToLoad = 0;

		this->shots = new Character*[numberOfShots];
	}

	~Scenario() {
		if (this->enemies != NULL) {
			delete this->enemies;
		}

		if (this->survivors != NULL) {
			delete this->survivors;
		}

		if (this->asteroids != NULL) {
			delete this->asteroids;
		}

		if (this->shots != NULL) {
			delete this->shots;
		}
	}

	int getNumberOfEnemies() { return this->numberOfEnemies; }
	int getNumberOfSurvivors() { return this->numberOfSurvivors; }
	int getNumberOfAsteroids() { return this->numberOfAsteroids; }
	int getNumberOfActiveEnemies() { return this->numberOfActiveEnemies; };
	int getNumberOfActiveSurvivors() { return this->numberOfActiveSurvivors; };
	int getNumberOfActiveAsteroids() { return this->numberOfActiveAsteroids; };
	int getNextEnemyToLoad() { return this->nextEnemyToLoad; }
	int getNextSurvivorToLoad() { return this->nextSurvivorToLoad; }
	int getNextAsteroidToLoad() { return this->nextAsteroidToLoad; }
	int getNextShotToLoad() { return this->nextShotToLoad; }

	Character** getEnemies() { return this->enemies; }
	Character** getSurvivors() { return this->survivors; }
	Character** getAsteroids() { return this->asteroids; }
	Character** getShots() { return this->shots; }

	// Configura o n�mero de inimigos e suas vari�veis de controle
	void setNumberOfEnemies(int numberOfEnemies) {
		if (this->enemies != NULL) { delete this->enemies; }
		this->enemies = new Character*[numberOfEnemies];
		this->numberOfEnemies = numberOfEnemies;
		this->nextEnemyToLoad = 0;
		this->numberOfActiveEnemies = numberOfEnemies;
	}

	// Configura o n�mero de sobreviventes e suas vari�veis de controle
	void setNumberOfSurvivors(int numberOfSurvivors) {
		if (this->survivors != NULL) { delete this->survivors; }
		this->survivors = new Character*[numberOfSurvivors];
		this->numberOfSurvivors = numberOfSurvivors;
		this->nextSurvivorToLoad = 0;
		this->numberOfActiveSurvivors = numberOfSurvivors;
	}

	// Configura o n�mero de aster�ides e suas vari�veis de controle
	void setNumberOfAsteroids(int numberOfAsteroids) {
		if (this->asteroids != NULL) { delete this->asteroids; }
		this->asteroids = new Character*[numberOfAsteroids];
		this->numberOfAsteroids = numberOfAsteroids;
		this->nextAsteroidToLoad = 0;
		this->numberOfActiveAsteroids = numberOfAsteroids;
	}

	void draw();
	void addEnemy(Character* newEnemy);
	void addSurvivor(Character* newSurvivor);
	void addAsteroid(Character* newAsteroid);
	void addShot(Character* newShot);
	void decNumberOfActiveSurvivors() { this->numberOfActiveSurvivors--; }
	void decNumberOfActiveAsteroids() { this->numberOfActiveAsteroids--; }
	void decNumberOfActiveEnemies() { this->numberOfActiveEnemies--; }
	void moveShots();
	void reset();
};

#endif