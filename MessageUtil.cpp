#define _CRT_SECURE_NO_DEPRECATE
#include <windows.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include "MessageUtil.h"

// A��es a serem executadas antes de exibir as mensagens
void MessageUtil::beforeShowMessage() {
	// Salva as matrizes
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
    glLoadIdentity();
	
	// Altera para projec�o ortogr�fica
	gluOrtho2D(0, 10.0, 0, 10.0);
}

// A��es a serem executadas depois de exibir as mensagens
void MessageUtil::afterShowMessage() {
	// Volta as matrizes
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

// Mostra estat�sticas do jogo
void MessageUtil::showGameInfo(int lives, int survivors, int remainingTime, bool isGameRunning) {
	beforeShowMessage();
	
	// Escreve os textos
	glColor3f(1, 1, 1);

	// N�mero de vidas restantes
	StringUtil::writeMessage(GLUT_BITMAP_9_BY_15, "Vidas: ", 7, 0.2f, 9.5f);
	char buffer[5];
	_itoa(lives, buffer, 10);
	StringUtil::writeMessage(GLUT_BITMAP_9_BY_15, buffer, 11, 0.9f, 9.5f);

	// Sobreviventes
	StringUtil::writeMessage(GLUT_BITMAP_9_BY_15, "Sobreviventes: ", 15, 1.3f, 9.5f);
	_itoa(survivors, buffer, 10);
	StringUtil::writeMessage(GLUT_BITMAP_9_BY_15, buffer, 11, 2.9f, 9.5f);

	// Tempo
	StringUtil::writeMessage(GLUT_BITMAP_9_BY_15, "Tempo: ", 7, 3.5f, 9.5f);
	_itoa(remainingTime, buffer, 10);
	StringUtil::writeMessage(GLUT_BITMAP_9_BY_15, buffer, 11, 4.2f, 9.5f);

	glColor3f(1, 0, 0);
	if (!isGameRunning && survivors == 0) {
		glColor3f(0, 1, 0);
		MessageUtil::showGameOver();
		MessageUtil::showCongrats();
	} else if (!isGameRunning && lives != 0) {
		MessageUtil::showGameStart();
	} else if ((remainingTime <= 0  || !isGameRunning) && lives == 0) {
		MessageUtil::showGameOver();
	} else if (remainingTime < 10) {
		MessageUtil::showShortTime();
	}

	afterShowMessage();
}

// Mostra mensagem para iniciar o jogo
void MessageUtil::showGameStart() {
	StringUtil::writeMessage(GLUT_BITMAP_9_BY_15, "Pressione a tecla 'n' para iniciar o jogo!", 42, 5, 9.5f);
}

// Mostra mensagem de final de jogo
void MessageUtil::showGameOver() {
	StringUtil::writeMessage(GLUT_BITMAP_9_BY_15, "FIM DE JOGO! Pressione 'j' novo jogo!", 37, 5, 9.5f);
}

// Mostra mensagem de jogo ganho
void MessageUtil::showCongrats() {
	StringUtil::writeMessage(GLUT_BITMAP_HELVETICA_18, "PARABENS! Voce ganhou o jogo!", 29, 3.2f, 9);
}

// Mostra aviso de tempo terminando
void MessageUtil::showShortTime() {
	StringUtil::writeMessage(GLUT_BITMAP_9_BY_15, "<= Atencao! O tempo esta acabando!", 34, 5, 9.5f);
}
	
// Mostra aviso de que um sobrevivente foi atingido por um tiro
void MessageUtil::showSurvivorShot() {
	beforeShowMessage();
	
	// Escreve texto
	glColor3f(0.9, 0.9, 0.1);

	// Mensagem de colis�o
	StringUtil::writeMessage(GLUT_BITMAP_HELVETICA_18, "AH, NAO! VOCE ATIROU EM UM SOBREVIVENTE!", 40, 2, 9);

	afterShowMessage();
}

// Mostra mensagem de colis�o da c�mera
void MessageUtil::showCollision() {
	beforeShowMessage();
	
	// Escreve texto
	glColor3f(0.9, 0.9, 0.1);

	// Mensagem de colis�o
	StringUtil::writeMessage(GLUT_BITMAP_HELVETICA_18, "AH, NAO! VOCE COLIDIU!", 22, 3.5f, 9);

	afterShowMessage();
}