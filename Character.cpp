#include <Windows.h>
#include <GL/gl.h>
#include "Character.h"


// Desenha o personagem
void Character::draw(){

	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

	if (this->characterType == ENEMY) {
		glColor3f(0.5, 0.0, 0.0);
	} else if (this->characterType == ASTEROID) {
		glColor3f(0.5, 0.5, 0.5);
	} else if (this->characterType == SURVIVOR) {
		glColor3f(0.0, 0.7, 0.0);
	} else {
		glColor3f(0.9, 0.9, 0.1);
	}
	
	//glColor3f(0.0, 1.0, 0.0);//verde
	glBegin(GL_POLYGON); // tampa
		glVertex3f(x,y+height,z);
		glVertex3f(x,y+height,z+depth);
		glVertex3f(x+width,y+height,z+depth);
		glVertex3f(x+width,y+height,z);
	glEnd();

	//glColor3f(0.0, 0.0, 1.0);//azul
	glBegin(GL_POLYGON); //frente
		glVertex3f(x, y, z);
		glVertex3f(x+width, y, z);
		glVertex3f(x+width, y+height, z);
		glVertex3f(x, y+height, z);
	glEnd();

	//glColor3f(0.0, 1.0, 1.0);
	glBegin(GL_POLYGON); //lado esquerdo
		glVertex3f(x, y, z);
		glVertex3f(x, y, z+depth);
		glVertex3f(x, y+height, z+depth);
		glVertex3f(x, y+height, z);
	glEnd();

	//glColor3f(1.0, 1.0, 1.0); //branco
	
	glBegin(GL_POLYGON); //lado direito
		glVertex3f(x+width, y, z);
		glVertex3f(x+width, y, z+depth);
		glVertex3f(x+width, y+height, z+depth);
		glVertex3f(x+width, y+height, z);
	glEnd();

//	glColor3f(1.0, 0.2, 0.0);//vermelho
	glBegin(GL_POLYGON); //fundo
		glVertex3f(x, y, z+depth);
		glVertex3f(x+width, y, z+depth);
		glVertex3f(x+width, y+height, z+depth);
		glVertex3f(x, y+height, z+depth);
	glEnd();

	angle += 0.2f;
	
}

// Inverte o movimento do personagem
void Character::reverseMovement(){
	switch (this->movDirection){
		case 1:
			this->movDirection = 2;
			break;
		case 2:
			this->movDirection = 1;
			break;
		case 3:
			this->movDirection = 4;
			break;
		case 4:
			this->movDirection = 3;
			break;
		default:
			break;
		}
}