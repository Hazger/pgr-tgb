#include <windows.h>
#include <GL/GL.h>
#include <GL/glut.h>
#include "Game.h"

Game* game= NULL;

void display(void){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
	game->tick();
	glFlush();
}

// Inicia um novo jogo
void initGame() {
	game = new Game();
	glLoadIdentity();
	gluLookAt(		
		game->getCamera()->getFx(),
		game->getCamera()->getFy(),
		game->getCamera()->getFz(),
		game->getCamera()->getFx()+game->getCamera()->getDx(),
		game->getCamera()->getFy()+game->getCamera()->getDy(),
		game->getCamera()->getFz()+game->getCamera()->getDz(),
		0,1,0
	);	
	glutPostRedisplay();
}

void init(){
	glClearColor(0, 0, 0, 0);

	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, 400, 400);
	gluPerspective(60, (double)WINDOW_WIDTH/(double)WINDOW_HEIGHT, 0.2, 200);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Inicia um novo jogo
	initGame();
}

// Manter o jogo exibindo o cen�rio
void idle() {
	if (game->isRunning()) {
		if (game->getTimer()->getElapsedTime() % 10 == 0) {
			glutPostRedisplay();
		}
	}
}

// Teclas especiais
void specialKeyboard(int key, int x, int y){
	if (game->isRunning()) {
		switch(key){
			case GLUT_KEY_LEFT:
				game->getCamera()->turnLeft();
				break;
			case GLUT_KEY_RIGHT:
				game->getCamera()->turnRight();
				break;
			case GLUT_KEY_UP:
				//std::cout << "seta cima" <<std::endl;
				game->getCamera()->goForward();
				break;
			case GLUT_KEY_DOWN:
				//std::cout << "seta pra baixo" << std::endl;
				game->getCamera()->goBack();
				break;
		}

		game->getCamera()->recalcSenCos();        

		glLoadIdentity();
		gluLookAt(
				game->getCamera()->getFx(),
				game->getCamera()->getFy(),
				game->getCamera()->getFz(), 
				game->getCamera()->getFx()+game->getCamera()->getDx(),
				game->getCamera()->getFy()+game->getCamera()->getDy(), 
				game->getCamera()->getFz()+game->getCamera()->getDz(), 
				0,1,0
		); 
		glutPostRedisplay();
	}
}

// Teclas comuns
void keyboard(unsigned char key, int x, int y){ 
	if (key == 'q' || key == 27) {
		exit(0);
	}
	if (game->isRunning()) {
		switch(key){
			case 'w':
					game->getCamera()->goForward();
					//std::cout << "w" << std::endl;
					break;
			case 's':
					game->getCamera()->goBack();
					//std::cout << "s" << std::endl;
					break;
			case 'a':
					game->getCamera()->turnLeft();
					//game->getCamera()->goLeft();
					//std::cout << "a" << std::endl;
					break;
			case 'd':
					game->getCamera()->turnRight();
					//game->getCamera()->goRight();
					//std::cout << "d" << std::endl;
					break;
			case ' ':
					//std::cout << "ESPACO" << std::endl;
					game->shoot();
					break;
			case 'x':
					game->getCamera()->turnAround();
					break;
			case 'm':
					game->toggleMapActive();
					break;
		}        
		game->getCamera()->recalcSenCos();

		glLoadIdentity();
		gluLookAt(
		game->getCamera()->getFx(),
		game->getCamera()->getFy(),
		game->getCamera()->getFz(), 
		game->getCamera()->getFx()+game->getCamera()->getDx(),
		game->getCamera()->getFy()+game->getCamera()->getDy(),
		game->getCamera()->getFz()+game->getCamera()->getDz(),
		0,1,0
		); 
		glutPostRedisplay();
	} else if ((game->getLives() != 0 && game->getScenario()->getNumberOfActiveSurvivors() != 0) && key == 'n') {
		game->init();
		glutPostRedisplay();
	} else if ((game->getLives() == 0 || game->getScenario()->getNumberOfActiveSurvivors() == 0) && key == 'j') {
		initGame();
	}
}

int main(int argc, char** argv){
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	glutInitWindowPosition(100, 10);
	glutCreateWindow("TGB - Jogo de Naves 3D");

	init();
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(specialKeyboard);
	glutIdleFunc(idle);
	glutMainLoop();
	return 0;
}