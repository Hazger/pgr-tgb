﻿JOGO DE NAVES 3D
Processamento Gráfico 2013/2
Bruna Mocelin, Tiago Ferreira

Objetivo: Resgatar todos os sobreviventes espalhados pelo jogo antes do final 
do tempo, evitando colisão com asteróides e naves inimigas.

O jogador possui 3 vidas e sempre que o tempo acaba, o jogador colide em naves 
inimigas ou asteróides, ou atira em um sobrevivente, este perde uma vida.

Legenda do jogo:
Sobreviventes: verde
Naves inimigas: vermelho escuro
Asteróides: cinza

Teclas de ação:
KEY_LEFT 'd':      Anda para a esquerda
KEY_RIGHT ou 'a':  Anda para a direita
KEY_UP ou 'w':     Anda para a frente
KEY_DOWN ou 's':   Anda para trás
' ':               Atira
'x':               Dá meia volta
'm':               Exibe/esconde o mapa do jogo