#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600
#define GAME_TIME 50

#ifndef GAME_H
#define GAME_H

#include <stdlib.h>
#include <GL/glut.h>
#include <iostream>
#include "Camera.h"
#include "Scenario.h"
#include "StringUtil.h"
#include "MessageUtil.h"
#include "Timer.h"

class Game{
private:
	Camera* camera;
	Scenario* scenario;
	int lives;
	Timer* timer;
	bool running;
	bool mapActive;
public:
	Game() {
		this->camera = new Camera();  // C�mera vai ser o jogador
		this->scenario = new Scenario();
		this->loadScenario();
		this->lives = 3;
		this->timer = new Timer();
		this->running = false;
		this->mapActive = false;
	}

	~Game() {
		if (this->camera != NULL) { delete this->camera; }
		if (this->scenario != NULL) { delete this->scenario; }
		if (this->timer != NULL) { delete this->timer; }
	}

	Camera* getCamera() { return camera; }
	Scenario* getScenario() {return scenario; }
	Timer* getTimer() { return timer; }
	int getLives() { return this->lives; }
	bool isRunning() { return running; }
	bool isMapActive() { return this->mapActive; }


	void tick(); // O tick � chamado no display e vai ser o in�cio de cada loop
	
	void setForw();    // Anda para frente
	void setBack();    // Anda para tr�s
	void setStrafeR(); // Strafe para direita
	void setStrafeL(); // Strafe para esquerda
	void loadScenario();
	void init();
	void reset();
	bool collision(float x1, float z1, float width1, float depth1, float x2, float z2, float width2, float depth2);
	int checkCollision(float x1, float z1, float width, float depth, bool isCamera);
	void shoot();
	void drawMap();
	void moveAsteroids();
	void toggleMapActive();
};

#endif