#ifndef INIMIGO_H
#define INIMIGO_H

enum CharacterType
{
	ENEMY,
	SURVIVOR,
	ASTEROID,
	SHOT
};

class Character{
private:
	float x;
	float y;
	float z;

	float width;
	float height;
	float depth;

	float angle;

	float dx; // Utilizado para o tiro
	float dz; // Utilizado para o tiro

	int movDirection;

	CharacterType characterType;
	boolean active;
public:
	Character(CharacterType characterType){
		this->x = 0;
		this->y = 0;
		this->z = 0;

		this->width = 0;
		this->height = 0;
		this->depth = 0;

		this->angle = 0;

		this->dx = 0;
		this->dz = 0;

		this->characterType = characterType;
		this->active = true;

		this->movDirection = 0;
	}

	void setX(float xi){ this->x = xi;}
	void setY(float yi){ this->y = yi;}
	void setZ(float zi){ this->z = zi;}
	void setWidth(float widthi){ this->width = widthi;}
	void setHeight(float heighti){ this->height = heighti;}
	void setDepth(float depthi){ this->depth = depthi;}
	void setActive(boolean active){ this->active = active;}
	void setDx(float dx) { this->dx = dx; }
	void setDz(float dz) { this->dz = dz; }
	void setMovDirection(int dm) { this->movDirection = dm; }

	float getX(){ return x;}
	float getY(){ return y;}
	float getZ(){ return z;}
	float getWidth(){ return width;}
	float getHeight(){ return height;}
	float getDepth(){ return depth;}
	boolean isActive() { return active;}
	float getDx() { return dx; }
	float getDz() { return dz; }
	int getMovDirection() { return movDirection; }
	
	void reverseMovement();	
	void draw();
};

#endif